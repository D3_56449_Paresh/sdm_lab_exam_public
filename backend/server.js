const express = require('express')
const db = require('./db')

const app = express()

app.use(express.json())

app.get('/emp/getAll',(request,response)=>{
    
    const connection = db.openConnection()

    const statement = `select * from emp`

    connection.query(statement,(error,emps)=>{
        connection.end()

        if(error)
        {
            response.send(error)
        }
        else
        {
            response.send(emps)
        }
    })
    
})

app.post('/emp/add',(request,response)=>{

    const {name,salary,age} = request.body
    const connection = db.openConnection()

    const statement = `insert into emp (name,salary,age) values ('${name}','${salary}','${age}')`

    connection.query(statement,(error,emp)=>{
        connection.end()

        if(error)
        {
            response.send(error)
        }
        else
        {
            result = {}
            result ['status'] = 'success'
            result ['data'] = emp
            response.send(result)
        }
    })
    
})

app.put('/emp/update/:empid',(request,response)=>{

    const {empid} = request.params
    const {salary} = request.body
    const connection = db.openConnection()

    const statement = `update emp set salary = '${salary}' where empid = '${empid}'`

    connection.query(statement,(error,result)=>{
        connection.end()

        if(error)
        {
            response.send(error)
        }
        else
        {
            response.send(result)
        }
    })
    
})

app.delete('/emp/delete/:empid',(request,response)=>{

    const {empid} = request.params
    
    const connection = db.openConnection()

    const statement = `delete from emp where empid = '${empid}'`

    connection.query(statement,(error,result)=>{
        connection.end()

        if(error)
        {
            response.send(error)
        }
        else
        {
            response.send(result)
        }
    })
    
})

app.listen(4000,()=>{
    console.log('server started on port 4000')
})